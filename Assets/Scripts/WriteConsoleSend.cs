﻿using UnityEngine;
using System.Collections;
using System;
using System.Net;
using System.Net.Mail;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.IO;

public class WriteConsoleSend : MonoBehaviour {


	public static void SendEmail () 
	{
		MailMessage mail = new MailMessage();

		mail.From = new MailAddress("pygmachia2016@gmail.com");
		mail.To.Add("studio@robertofazio.com");
		mail.To.Add ("pygmachia2016@gmail.com");
		mail.To.Add("collina.michel@gmail.com");
		mail.Subject = "PYGMACHIA MaTa - Unity Log Events";
		mail.Body = "Send a Log Event Error from Pygmachia ";

		SmtpClient smtpServer = new SmtpClient("smtp.gmail.com");
		smtpServer.Port = 587;
		smtpServer.Credentials = new System.Net.NetworkCredential ("pygmachia2016@gmail.com", "MaTaPygmachia") as ICredentialsByHost;

		smtpServer.EnableSsl = true;
		ServicePointManager.ServerCertificateValidationCallback = 
			delegate(object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) 
		{ return true; };

		// attachment pygmachia.log in asset
		System.Net.Mail.Attachment attachment;
		attachment = new System.Net.Mail.Attachment("PygmachiaDev.log");
		mail.Attachments.Add(attachment);

		smtpServer.Send(mail);
		Debug.Log("- Success - Email sent!");
		WriteLogFile ("- Success - Email sent!");

	}

	public static void WriteLogFile(string msg)
	{
		try
		{
			StreamWriter w = new StreamWriter("PygmachiaDev.log", true);
			DateTime today = DateTime.Now;
			string line = string.Format("{0:00}-{1:00}-{2:0000}_{3:00}:{4:00}:{5:00} >{6}\n",
				today.Day, today.Month, today.Year, today.Hour, today.Minute, today.Second, msg);

			w.WriteLine(line);
			w.Close();
		}
		catch (Exception e)
		{
			UnityEngine.Debug.LogWarning(e.Message);
		}
	}
		


		
}
