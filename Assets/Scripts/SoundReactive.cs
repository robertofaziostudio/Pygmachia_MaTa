﻿/*
 * Roberto Fazio Studio 07.2014
 * Raw Microphone Sound Analisys 
 *
 * http://answers.unity3d.com/questions/394158/real-time-microphone-line-input-fft-analysis.html
 * http://answers.unity3d.com/questions/157940/getoutputdata-and-getspectrumDatadata-they-represent-t.html
 * http://forum.unity3d.com/threads/119595-Using-device-microphone-to-interact-with-objects
 * http://www.kaappine.fi/tutorials/fundamental-frequencies-and-detecting-notes/
 * 
 */
using UnityEngine;
using System.Collections;

public class SoundReactive : MonoBehaviour 
{
	public static int 		deviceNum;
	public static float[] 	spectrumData; // Array of frequencies
	public static int 		SampleCount = 1024;
	public static int 		samplerate = 11024;
	public float 			loudness;
	public float			frequency;
	public float		    sensitivity = 200.0f;

	void Start () 
	{
		spectrumData = new float[SampleCount];
	}
	
	void Update () 
	{
		AudioManager.PygmachiaMainAudioSource.GetOutputData(spectrumData, 0);
		AudioManager.PygmachiaMainAudioSource.GetSpectrumData(spectrumData, 0, FFTWindow.BlackmanHarris);

		loudness = GetAveragedVolume() * sensitivity;
		frequency = GetFundamentalFrequency();
	}

	float GetAveragedVolume() 
	{
		float a = 0;
		foreach (float s in spectrumData) 
		{
			a += Mathf.Abs (s);
		}
		return a / 256;
	}

	float GetFundamentalFrequency() 
	{
		float fundamentalFrequency = 0.0f;
		float s = 0.0f;
		int i = 0;
		for (int j = 1; j < SampleCount; j++) 
		{
			if (s < spectrumData [j]) 
			{
				s = spectrumData [j];
				i = j;
			}
		}
		fundamentalFrequency = i * samplerate / SampleCount;
		return fundamentalFrequency;
	}

	void GetMicrophone()
	{
		string[] inputDevices = new string[Microphone.devices.Length];
		for(int i = 0; i < Microphone.devices.Length; i++)
		{	
			inputDevices[i] = Microphone.devices[i].ToString();
			print("Audio Device : " + inputDevices[i]);
		}

		//string CurrentAudioInput = Microphone.devices[deviceNum].ToString();
	}
}
