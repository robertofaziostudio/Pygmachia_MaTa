﻿using UnityEngine;
using System.Collections;
using System.IO;
using System;

public class FreezingHack : MonoBehaviour 
{
	public static int counter;
	public static StreamWriter streamFile;
	private float waitTime = 5.0f;

	void Start()
	{
		if (File.Exists ("Freeze.txt")) 
			File.Delete ("Freeze.txt");
		
		StartCoroutine (StartCounting (waitTime));
	}
		
	public void reStart ()
	{
		StartCoroutine (StartCounting (waitTime));
	}


	public IEnumerator StartCounting(float _waitTime)
	{
		SetTiming ();

		yield return new WaitForSeconds(waitTime);

		reStart ();
	}


	public static void SetTiming()
	{
		counter += 10;
		streamFile = new StreamWriter("Freeze.txt", true);
		streamFile.Write(counter + "\n" );
		streamFile.Close();
		//Debug.LogError("counter " + counter);
	}

	void OnApplicationQuit()
	{
		//if (File.Exists ("Freeze.txt")) 
		//	File.Delete ("Freeze.txt");
	}
}
