﻿using UnityEngine;
using System.Collections;

public class Shield : MonoBehaviour 
{
	public Material ShieldMaterial;
	public  Color32 shieldCol;

	public static bool checkShieldActive;

	public static bool setOn;
	public static float speed = 1.8f;
	public static float v;

	public float minimum = 0.0f;
	public float maximum =  1.0f;
	static float t = 0.0f;
	private float timeFadeOut = 0;

	public MovCamera mCam;

	void Start () 
	{
		checkShieldActive = false;
		setOn = false;
	}

	void Update () 
	{
		GetShieldActive ();

		if(v != 0)
		{
			Medusa.punchRight = false;
			MedusaSx.punchLeft = false;
			FadeMaterialEffect.checkLeft = false;
			FadeMaterialEffect.checkRight = false;
			MovCamera.movimenti = MovCamera.MovimentiCamera.MedusaShield;
			Medusa.dAccMag = 0;
			MedusaSx.dAccMag = 0;
			
		}


	}
		
	void GetShieldActive()
	{
		this.GetComponent<Renderer> ().material.color = new Color (1, 1, 1, (byte)v);

		if (v == minimum) 
		{
			this.GetComponent<BoxCollider> ().enabled = false;
			this.gameObject.GetComponent<Renderer> ().material.SetColor ("_EmissionColor", Color.red);
		}
		else
			this.GetComponent<BoxCollider>().enabled = true;
		

		if (Medusa.GetShield () == true) 
		{
			setOn = true;

			this.gameObject.AddComponent<AudioSource> ();
			this.gameObject.GetComponent<AudioSource> ().clip = Resources.Load ("Audio/MaTa-SoundEffect/MaTa-SoundEffect-Scudo") as AudioClip;
			this.gameObject.GetComponent<AudioSource> ().Play ();
			this.gameObject.GetComponent<AudioSource> ().loop = false;

			//MovCamera.movimenti = MovCamera.MovimentiCamera.MedusaShield;
			mCam.StartMovment(MovCamera.MovimentiCamera.MedusaShield);
		}
			
		
		// START SHIELD VIEW
		if (setOn == true) 
		{
			maximum = 255;
			FadeMaterialEffect.SetStartBlack();

		
		}
		else if (setOn == false)
		{
			maximum = 0; 

		}

		t += speed * Time.deltaTime;
		v = Mathf.Lerp(minimum, maximum, t);

		if (t > 1.0f)
		{
			float temp = maximum;
			maximum = minimum;
			minimum = temp;
			t = 0.0f;
			setOn = false;
			checkShieldActive = false;

		}

			
	}



	// COLLISIONE DELLO SHIELD CON L'OGGETTO CENTRALE
	void OnCollisionEnter(Collision collider) 
	{
		if (collider.gameObject.tag == "CENTER" && this.GetComponent<BoxCollider>().enabled == true)  
		{
			// *** OGGETTO STUFF COLLISION ***
			timeFadeOut += Time.deltaTime;

			collider.gameObject.GetComponent<Renderer> ().material.SetColor ("_EmissionColor", Color.blue);
			collider.gameObject.GetComponent<BoxCollider> ().enabled = false;
	
			this.gameObject.AddComponent<AudioSource> ();
			this.gameObject.GetComponent<AudioSource> ().clip = Resources.Load ("Audio/MaTa-SoundEffect/MaTa-SoundEffect-Colpo-Oggetti") as AudioClip;
			this.gameObject.GetComponent<AudioSource> ().Play ();
			this.gameObject.GetComponent<AudioSource> ().loop = false;

			// *** SHIELD STUFF COLLISION ***
			this.gameObject.GetComponent<Renderer> ().material.SetColor ("_EmissionColor", Color.blue);

			collider.gameObject.GetComponent<OggettoBehiavor> ().enabled = true;
			//Debug.LogError ("CENTER" + collider.gameObject.name);
			DMXManager.actualMode = DMXManager.SetDmxMode.BluePulse;


		}


	}
}
