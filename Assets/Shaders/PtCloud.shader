﻿Shader "Custom/VertexColor" {
 
    Properties{
    	_minVertSize ("MinVertSize", Float) = 100.0
    	_maxVertSize ("MaxVertSize", Float) = 200.0
    	_colorDepth ("ColorDepth", Float) = 300.0
    	_minColor ("MinColor", Vector) = (0,0,1)
    	_maxColor ("MaxColor", Vector) = (1,0,0)
    }
    
    SubShader {
    Pass {
        LOD 200
         
        CGPROGRAM
        #pragma vertex vert
        #pragma fragment frag
 		
 		float _minVertSize;
 		float _maxVertSize;
 		float _colorDepth;
 		float3 _minColor;
 		float3 _maxColor;
 
        struct VertexInput {
            float4 v : POSITION;
            float4 color: COLOR;
        };
         
        struct VertexOutput {
            float4 pos : SV_POSITION;
            float4 col : COLOR;
            float size : PSIZE0;
        };
         
        VertexOutput vert(VertexInput v) {
         
            VertexOutput o;
            o.pos = mul(UNITY_MATRIX_MVP, v.v);
            float pct = v.v[2]/_colorDepth;
            o.col[0] = lerp(_minColor[0],_maxColor[0],pct);
            o.col[1] = lerp(_minColor[1],_maxColor[1],pct);;
            o.col[2] = lerp(_minColor[2],_maxColor[2],pct);;
            o.size = lerp(_minVertSize,_maxVertSize,pct);

                   
            return o;
        }
         
        float4 frag(VertexOutput o) : COLOR {
            return o.col;
        }
 
        ENDCG
        } 
    }
 
}
